import pymysql


class Stu(object):
    def __init__(self):
        self.conn = pymysql.connect(
            host='localhost',
            port=3306,
            user='root',
            password='xuan',
            database='conne',
            charset='utf8'
        )
        self.cs = self.conn.cursor()

    def catch(self):
        sql = "select * from stu"
        self.cs.execute(sql)
        ss = self.cs.fetchall()
        for i in ss:
            print(i)

    # def start(self):
    #     self.conn = pymysql.connect(
    #         host='localhost',
    #         port=3306,
    #         user='root',
    #         password='xuan',
    #         database='conne',
    #         charset='utf8'
    #     )
    #
    #     self.cs = self.conn.cursor()

    def close(self):
        self.cs.close()
        self.conn.close()


def main():
    stu = Stu()
    # stu.start()
    stu.catch()
    stu.close()


if __name__ == '__main__':
    main()
