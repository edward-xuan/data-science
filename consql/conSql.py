import pymysql


def main():
    # 创建Connection链接
    conn = pymysql.connect(
        host='localhost',
        port=3306,
        user='root',
        password='xuan',
        database='conne',
        charset='utf8'
    )
    # 获取Cursor对象
    cs1 = conn.cursor()

    # select 语句
    count = cs1.execute("select * from stu")
    print(count)
    ss = cs1.fetchall()

    for i in ss:
        print(i)

    # for i in range(count):
    #     # 获取查询结果
    #     result = cs1.fetchone()
    #     # 打印查询结果
    #     print(result)

    cs1.close()
    conn.close()


if __name__ == '__main__':
    main()
