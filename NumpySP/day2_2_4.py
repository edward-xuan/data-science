"""
数组的变形
"""
import numpy

if __name__ == '__main__':
    # 1 ~ 9 放入 3 * 3的矩阵
    grid = numpy.arange(1, 10).reshape((3, 3))
    print(grid)
    x = numpy.array([1, 2, 3])
    # 通过变形获取行向量
    print(x.reshape((1, 3)))
    # 通过newaxis获取行向量
    print(x[numpy.newaxis, :])
    # 通过变形获取列向量
    print(x.reshape((3, 1)))
    # 通过newaxis获取列向量
    print(x[:, numpy.newaxis])
