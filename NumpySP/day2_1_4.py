"""
从Python列表创建数组
"""
# import array
import numpy


if __name__ == '__main__':
    # L = list(range(10))
    # A = array.array('i', L)
    # print(A)

    print(numpy.array([1, 4, 2, 5, 3]))

    # 向上转型
    print(numpy.array([3.14, 4, 2, 3]))

    # 使用 dtype 关键字设置类型
    print(numpy.array([1, 2, 3, 4], dtype='float32'))

    # 嵌套列表构成的多维数组
    print(numpy.array([range(i, i + 3) for i in [2, 4, 6]]))
