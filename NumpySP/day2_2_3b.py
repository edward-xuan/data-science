"""
数组切片:获取子数组b
"""
import numpy

if __name__ == '__main__':
    numpy.random.seed(0)
    # 多维子数组
    x2 = numpy.random.randint(10, size=(3, 4))
    print(x2)
    print(x2[:2, :3])  # 二行 三列
    print(x2[:, ::2])  # 所有行,隔一列
    print((x2[::-1, ::-1]))  # 逆向

    print(x2[:, 0])  # 第一列
    print(x2[0, :])  # 第一行
    print(x2[0])  # 第一行 等于x2[0, :]

    print(x2)
    # 这种赋值，子数组的元素更改后，原数组相应的元素也会进行相同的修改
    x2_sub = x2[:2, :2]
    print(x2_sub)
    x2_sub[0, 0] = 1
    print(x2_sub)
    print(x2)
    # 这种赋值，子数组的元素更改后，原数组相应的元素保持不变
    x2_sub_copy = x2[:2, :2].copy()
    print(x2_sub_copy)
    x2_sub_copy[0, 0] = 99
    print(x2_sub_copy)
    print(x2)
