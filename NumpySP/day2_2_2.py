"""
数组索引:获取单个元素
"""
import numpy

if __name__ == '__main__':
    numpy.random.seed(0)  # 设置随机数种子
    x1 = numpy.random.randint(10, size=6)  # 一维数组
    x2 = numpy.random.randint(10, size=(3, 5))  # 二维数组
    x3 = numpy.random.randint(10, size=(3, 4, 5))  # 三维数组

    print(x1[-1], "\t", x2[-1, -1], "\t", x3[-1, -1, -1])
    print(x1)
    x1[4] = 999
    print(x1)
