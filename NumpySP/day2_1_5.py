"""
从头创建数组
"""
import numpy


if __name__ == '__main__':
    # len = 10 值全为0 的整型数组
    print(numpy.zeros(10, dtype=int))
    # len = 10 值全为1 的整形数组
    print(numpy.ones(10, dtype=float))
    # 3 * 5 的矩阵 值全为 3.14
    print(numpy.full((3, 5), 3.14))
    # 3 * 5 的矩阵 值全为 1 浮点型
    print(numpy.full((3, 5), 1, dtype=float))
    # 0 ~ 20 步长为 2 和range()函数相似
    print(numpy.arange(0, 20, 2))
    # 一个有五个元素的数组，5个数均匀分布在 0 ~ 1
    print(numpy.linspace(0, 1, 5))
    # 一个有五个元素的数组，5个数均匀分布在 0 ~ 15
    print(numpy.linspace(0, 15, 5))
    # 3 * 3 0 ~ 1 均匀随机数组
    print(numpy.random.random((3, 3)))
    # 3 * 3 均值 0 标准差 1 状态随机数组
    print(numpy.random.normal(0, 1, (3, 3)))
    # 3 * 3 [0, 10) 之间的随机整型数组
    print(numpy.random.randint(0, 10, (3, 3)))
    # 3 * 3 单位矩阵
    print(numpy.eye(3))
    print(numpy.eye(4))
    # 数组内容为内存空间的任意值 长度为6
    print(numpy.empty(6))
