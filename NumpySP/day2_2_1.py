"""
NumPy数组的属性
"""
import numpy


if __name__ == '__main__':
    numpy.random.seed(0)  # 设置随机数种子
    x1 = numpy.random.randint(10, size=6)  # 一维数组
    x2 = numpy.random.randint(10, size=(3, 5))  # 二维数组
    x3 = numpy.random.randint(10, size=(3, 4, 5))  # 三维数组
    # print(x1)
    print("x1 ndim 数组维度\t", x1.ndim)
    # print(x2)
    print("x2 shape 数组每个维度大小\t", x2.shape)
    # print(x3)
    print("x2 size 数组总大小\t", x3.size)
    print("x1 dtype 数据类型", x3.dtype)
    print("itemsize 每个数组元素大小", x3.itemsize, "bytes")
    print("nbytes 数组总大小", x3.nbytes, "bytes")
