vec = [[1, 2], [3, 4]]

c1 = [col for row in vec for col in row]

c2 = []

for row in vec:
    for col in row:
        c2.append(col)

print(c1)
print(c2)

c3 = [[row[i] for row in vec] for i in range(len(vec[0]))]

c4 = []

x = {1: 2, 2: 3, 3: 5, object: object}
print(x.get(999, "滚，没有"))

x = [
    [1, 3, 3],
    [2, 3, 1]
]
print(sorted(x, key=lambda item: item[0] + item[2]))

