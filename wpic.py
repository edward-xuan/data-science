import jieba
import wordcloud
from matplotlib import pyplot

if __name__ == '__main__':
    filename = "yes_ministerA.txt"

    with open(filename, encoding='utf-8') as chf:
        txt = chf.read()
        # print(txt)

    mt = " ".join(jieba.cut(txt))
    # print(mt)
    cloud = wordcloud.WordCloud(font_path="PingFang SC.ttc").generate(mt)

    pyplot.imshow(cloud, interpolation='bilinear')
    pyplot.axis('off')
    pyplot.rcParams['figure.figsize'] = (10.0, 5.0)
    pyplot.rcParams['savefig.dpi'] = 100000
    pyplot.rcParams['figure.dpi'] = 60000
    pyplot.show()

