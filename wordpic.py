import wordcloud
from matplotlib import pyplot

if __name__ == '__main__':
    filename = "yes_minister.txt"

    with open(filename, encoding='utf-8') as word:
        text = word.read()
        # print(text)

    cloud = wordcloud.WordCloud().generate(text)

    pyplot.imshow(cloud, interpolation='bilinear')
    pyplot.axis('off')
    pyplot.show()


